from django.db import models


class Request(models.Model):
    date_time = models.DateTimeField
    flight = models.CharField(max_length=10)
    isopen = models.BooleanField


class User(models.Model):
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    email = models.EmailField
    phone = models.CharField(max_length=10)
    address = models.CharField(max_length=100)
    request = models.ForeignKey(Request, on_delete=models.CASCADE)


class Match(models.Model):
    v = User  # volunteer
    s = User  # student
    request = Request



